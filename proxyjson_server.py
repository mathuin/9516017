from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet.error import CannotListenError
from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor
import simplejson as json

# Ports available for proxy purposes.
low_port = 5000
high_port = 5100


class EchoFactory(Factory):
    def __init__(self, proxy):
        self.proxy = proxy

    def buildProtocol(self, addr):
        return Echo(self)


class Echo(Protocol):
    def __init__(self, factory):
        self.factory = factory

    def connectionMade(self):
        self.transport.write(self.factory.proxy.output)
        self.transport.loseConnection()

    def connectionLost(self, reason):
        self.factory.proxy.open_port.stopListening()
        self.factory.proxy.ports.append(self.factory.proxy.dport)


class ProxyFactory(Factory):
    def buildProtocol(self, addr):
        return Proxy(self)


class Proxy(Protocol):
    ports = [x for x in xrange(low_port, high_port)]
    in_use = []

    def __init__(self, factory):
        self.dport = None
        self.factory = factory

    def connectionMade(self):
        while not self.dport:
            try:
                self.dport = Proxy.ports.pop()
            except IndexError:
                self.transport.write("No ports available!")
                self.transport.abortConnection()
            try:
                self.endpoint = TCP4ServerEndpoint(reactor, self.dport,
                                                   backlog=1)
            except CannotListenError:
                self.dport = None

    def dataReceived(self, data):
        self.output = data
        # abort connection if json is invalid?
        self.transport.write(str(self.dport))
        self.endpoint.listen(EchoFactory(self)).addCallback(self.set_open_port)
        self.transport.loseConnection()

    def set_open_port(self, result):
        self.open_port = result
