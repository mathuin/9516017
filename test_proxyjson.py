from proxyjson_server import ProxyFactory, EchoFactory
from twisted.trial import unittest
from twisted.test import proto_helpers
from mock import patch, MagicMock


class ProxyJsonFactoryTestCase(unittest.TestCase):
    def setUp(self):
        factory = ProxyFactory()
        self.proto = factory.buildProtocol(('127.0.0.1', 0))
        self.tr = proto_helpers.StringTransport()
        self.proto.makeConnection(self.tr)

    # The following test mocks up a connection to the port returned by
    # the proxy.  The object that listens for the connection has the
    # correct return value in it, so we just check for that value.

    @patch('proxyjson_server.Echo')
    def test_proxyjson(self, proxy_echo):
        instring = 'hi'
        self.proto.dataReceived(instring)
        inport = int(self.tr.value())
        echof = EchoFactory(self.proto)
        self.echop = echof.buildProtocol(('127.0.0.1', inport))
        args, kwargs = proxy_echo.call_args
        self.assertEqual(args[0].proxy.output, instring)
